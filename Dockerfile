FROM node:lts-alpine as build-stage
WORKDIR /app
COPY package*.json ./
RUN npm install
COPY . .
RUN npm run build

FROM nginx:1.20.1-alpine as production-stage
RUN curl -fsSL https://deb.nodesource.com/setup_${NODE_VERSION}.x | bash - \
    && apt-get update \
    && apt-get autoremove \
    && apt-get install -y nodejs

RUN apt-get update && apt-get install -y npm
COPY ./nginx.conf /etc/nginx/nginx.conf


WORKDIR /app
COPY ./dist .
EXPOSE 8080:8080
CMD ["nginx", "-g", "daemon off;"]

# RUN npm run serve
# CMD ["npm", "run", "serve"]
# CMD ["nginx", "-g", "daemon off;"]
# http://localhost:8080/

# RUN docker build -t dockerize-vuejs .
# RUN docker run -it -p 8080:80 --rm --name dockerize-vue dockerize-vuejs